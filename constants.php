<?php
/**
 * defining basic constants so phpstorm would shut up about it
 * also for autocompletion
 */

// wcf
const WCF_N = 1;
const WCF_DIR = '';

// chat.command.rps
const CHAT_RPS_COOLDOWN = 30;