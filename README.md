net.dalang.chat.command.rps

Start new RPS game
/rps <rock, paper, scissors> [username challengee]
/rps <1, 2, 3> [username challengee]

Start new RPSLS game
/rpsls <rock, paper, scissors, lizard, spock> [username challengee]
/rpsls <1, 2, 3, 4, 5> [username challengee]


Challenge RPS game
/rps <rock, paper, scissors> <username challenger>
/rps <1, 2, 3> <username challenger>

Challenge RPSLS game
/rpsls <rock, paper, scissors, lizard, spock> <username challenger>
/rpsls <1, 2, 3, 4, 5> <username challenger>