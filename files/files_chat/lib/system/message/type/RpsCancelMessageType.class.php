<?php
namespace chat\system\message\type;
use chat\data\message\Message;
use chat\data\room\Room;
use wcf\data\user\UserProfile;
use wcf\system\WCF;

/**
 * rps cancel message type
 *
 * @author		Daniel (Keito) Lang
 * @copyright	2014-2023 da-lang.net
 * @license		da-lang.net Commercial License <https://da-lang.net/license/commercial.txt>
 * @package		net.dalang.chat.command.rps
 */
class RpsCancelMessageType implements IMessageType {
	use TDefaultPayload;
	use TCanSeeCreator;
	
	/**
	 * @inheritdoc
	 */
	public function getJavaScriptModuleName():string {
		return 'Dalang/Chat/MessageType/RpsCancel';
	}
	
	/**
	 * @inheritdoc
	 */
	public function canSeeInLog(Message $message, Room $room, ?UserProfile $user = null):bool {
		return false;
	}
}