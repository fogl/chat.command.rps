<?php
namespace chat\system\message\type;
use chat\data\message\Message;
use chat\data\room\Room;
use wcf\data\user\UserProfile;

/**
 * rps challenge info message type
 *
 * @author		Daniel (Keito) Lang
 * @copyright	2014-2023 da-lang.net
 * @license		da-lang.net Commercial License <https://da-lang.net/license/commercial.txt>
 * @package		net.dalang.chat.command.rps
 */
class RpsChallengeInfoMessageType implements IMessageType {
	use TDefaultPayload;
	use TCanSeeCreator;
	
	/**
	 * @inheritdoc
	 */
	public function getJavaScriptModuleName() {
		return 'Dalang/Chat/MessageType/RpsChallengeInfo';
	}
	
	/**
	 * @inheritdoc
	 */
	public function canSeeInLog(Message $message, Room $room, ?UserProfile $user = null):bool {
		return false;
	}
}