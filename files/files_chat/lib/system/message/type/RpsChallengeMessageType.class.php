<?php
namespace chat\system\message\type;
use chat\data\message\Message;
use wcf\data\user\UserProfile;
use wcf\system\WCF;

/**
 * rps challenge message type
 *
 * @author		Daniel (Keito) Lang
 * @copyright	2014-2023 da-lang.net
 * @license		da-lang.net Commercial License <https://da-lang.net/license/commercial.txt>
 * @package		net.dalang.chat.command.rps
 */
class RpsChallengeMessageType implements IMessageType, IDeletableMessageType {
	use TCanSeeInSameRoom;
	use TDefaultPayload;
	
	/**
	 * @inheritdoc
	 */
	public function getJavaScriptModuleName() {
		return 'Dalang/Chat/MessageType/RpsChallenge';
	}
	
	/**
	 * @inheritdoc
	 */
	public function canDelete(Message $message, ?UserProfile $user = null) {
		if ($user === null) $user = new UserProfile(WCF::getUser());
		
		return $user->getPermission('mod.chat.canDelete');
	}
}