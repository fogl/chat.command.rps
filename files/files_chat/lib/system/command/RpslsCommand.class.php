<?php
namespace chat\system\command;
use wcf\data\user\UserProfile;
use wcf\system\exception\PermissionDeniedException;
use wcf\system\exception\UserInputException;
use wcf\system\WCF;

/**
 * rpsls command
 *
 * @author		Daniel (Keito) Lang
 * @copyright	2014-2023 da-lang.net
 * @license		da-lang.net Commercial License <https://da-lang.net/license/commercial.txt>
 * @package		net.dalang.chat.command.rps
 */
class RpslsCommand extends RpsCommand {
	/**
	 * @inheritdoc
	 */
	public $allowedSymbols = [
		'rock',
		'paper',
		'scissors',
		'lizard',
		'spock'
	];
	
	/**
	 * @inheritdoc
	 */
	public $mode = 'rpsls';
	
	/**
	 * @inheritdoc
	 */
	public function getJavaScriptModuleName() {
		return 'Dalang/Chat/Command/Rpsls';
	}
	
	/**
	 * @inheritdoc
	 */
	public function validatePermission(UserProfile $user) {
		if (!$user->getPermission('user.chat.canPlayRpsls')) {
			throw new PermissionDeniedException();
		}
	}
	
	/**
	 * validatePermissionOtherUser
	 */
	public function validatePermissionOtherUser() {
		if (!$this->challengedUser->getPermission('user.chat.canPlayRpsls')) {
			throw new UserInputException('message', WCF::getLanguage()->get('chat.error.rps.userCannotPlayRpsls'));
		}
	}
}