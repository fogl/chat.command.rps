<?php
namespace chat\system\command;
use chat\data\message\MessageAction;
use chat\data\room\Room;
use wcf\data\user\UserProfile;
use wcf\system\exception\PermissionDeniedException;
use wcf\system\exception\UserInputException;
use wcf\system\flood\FloodControl;
use wcf\system\style\FontAwesomeIcon;
use wcf\system\user\storage\UserStorageHandler;
use wcf\system\WCF;

/**
 * rps command
 *
 * @author		Daniel (Keito) Lang
 * @copyright	2014-2023 da-lang.net
 * @license		da-lang.net Commercial License <https://da-lang.net/license/commercial.txt>
 * @package		net.dalang.chat.command.rps
 */
class RpsCommand extends AbstractCommand implements ICommand {
	use TNeedsUser;
	
	/**
	 * challengedUser
	 * @var UserProfile
	 */
	public $challengedUser;
	
	/**
	 * symbol
	 * @var string
	 */
	public $symbol = '';
	
	/**
	 * allowedSymbols
	 * @var string[]
	 */
	public $allowedSymbols = [
		'rock',
		'paper',
		'scissors'
	];
	
	/**
	 * mode
	 * differs between rps and rpsls mode
	 * @var string
	 */
	public $mode = 'rps';
	
	/**
	 * @inheritdoc
	 */
	public function getJavaScriptModuleName() {
		return 'Dalang/Chat/Command/Rps';
	}
	
	/**
	 * @inheritdoc
	 */
	public function validate($parameters, Room $room, ?UserProfile $user = null) {
		if ($user === null) $user = new UserProfile(WCF::getUser());
		
		if (!$room->canWritePublicly($user)) {
			throw new PermissionDeniedException();
		}
		
		$this->validatePermission($user);
		
		$this->symbol = $this->assertParameter($parameters, 'symbol');
		
		switch ($this->symbol) {
			case 'cancel':
				//validate canceling challenge
				$currentChallenge = UserStorageHandler::getInstance()->getField('chatRpsChallenge', $user->userID) ?? [];
				if (empty($currentChallenge)) {
					throw new UserInputException('message', WCF::getLanguage()->getDynamicVariable('chat.error.rps.noActiveChallenge'));
				}
				break;
				
			default:
				$this->challengedUser = new UserProfile($this->assertUser($this->assertParameter($parameters, 'username')));
				
				// anti-spam
				$lastCommand = FloodControl::getInstance()->getLastTime('net.dalang.chat.command.rps');
				if ($lastCommand !== null && CHAT_RPS_COOLDOWN > 0) {
					$timeDiff = TIME_NOW - $lastCommand;
					if ($timeDiff <= CHAT_RPS_COOLDOWN) {
						throw new UserInputException('message', WCF::getLanguage()->getDynamicVariable('chat.error.rps.cooldown', ['restTime' => CHAT_RPS_COOLDOWN - $timeDiff]));
					}
				}
				
				
				// validate open challenge
				$currentChallenge = UserStorageHandler::getInstance()->getField('chatRpsChallenge', $user->userID) ?? [];
				if (!empty($currentChallenge)) {
					$currentChallengeVars = unserialize($currentChallenge);
					throw new UserInputException('message', WCF::getLanguage()->getDynamicVariable('chat.error.rps.ongoingChallenge'));
				}
				
				// validate symbol
				if (!in_array($this->symbol, $this->allowedSymbols)) {
					throw new UserInputException('message', WCF::getLanguage()->get('chat.error.rps.invalidSymbol'));
				}
				
				// validate user
				if ($user->userID == $this->challengedUser->userID) {
					throw new UserInputException('message', WCF::getLanguage()->get('chat.error.rps.challengeSelf'));
				}
				
				if ($user->isIgnoredByUser($this->challengedUser->userID)) {
					throw new UserInputException('message', WCF::getLanguage()->get('chat.error.rps.userIgnoresYou'));
				}
				if ($user->isIgnoredUser($this->challengedUser->userID)) {
					throw new UserInputException('message', WCF::getLanguage()->get('chat.error.rps.youIgnoreUser'));
				}
				
				$this->validatePermissionOtherUser();
				
				$chatUser = new \chat\data\user\User($this->challengedUser->getDecoratedObject());
				if (!$chatUser->isInRoom($room)) {
					throw new UserInputException('message', WCF::getLanguage()->get('chat.error.rps.notInRoom'));
				}
				
				$challengedUserSymbol = UserStorageHandler::getInstance()->getField('chatRpsChallenge', $this->challengedUser->userID);
				if ($challengedUserSymbol !== null) {
					$challengedUserSymbolVar = unserialize($challengedUserSymbol);
					if ($challengedUserSymbolVar['userID'] != $user->userID) {
						throw new UserInputException('message', WCF::getLanguage()->get('chat.error.rps.userHasActiveChallenge'));
					}
				}
				break;
		}
	}
	
	/**
	 * validatePermission
	 * @param	UserProfile	$user
	 */
	public function validatePermission(UserProfile $user) {
		if (!$user->getPermission('user.chat.canPlayRps')) {
			throw new PermissionDeniedException();
		}
	}
	
	/**
	 * validatePermissionOtherUser
	 */
	public function validatePermissionOtherUser() {
		if (!$this->challengedUser->getPermission('user.chat.canPlayRps')) {
			throw new UserInputException('message', WCF::getLanguage()->get('chat.error.rps.userCannotPlayRps'));
		}
	}
	
	/**
	 * @inheritdoc
	 */
	public function execute($parameters, Room $room, ?UserProfile $user = null) {
		if ($user === null) $user = new UserProfile(WCF::getUser());
		
		if ($this->symbol == 'cancel') {
			UserStorageHandler::getInstance()->reset([$user->userID], 'chatRpsChallenge');
			
			$action = new MessageAction([], 'create', [
				'data' => [
					'roomID' => $room->roomID,
					'userID' => $user->userID,
					'username' => $user->username,
					'time' => TIME_NOW,
					'objectTypeID' => $this->getMessageObjectTypeID('net.dalang.chat.messageType.rps.cancel'),
					'payload' => serialize([
						'rpsMode' => $this->mode == 'rps'
					])
				],
				'updateTimestamp' => true,
				'grantPoints' => false
			]);
			$action->executeAction();
			
			return;
		}
		
		FloodControl::getInstance()->registerContent('net.dalang.chat.command.rps');
		
		// check if this call is a new challenge or the response to one
		$otherSymbolStorage = UserStorageHandler::getInstance()->getField('chatRpsChallenge', $this->challengedUser->userID);
		if ($otherSymbolStorage !== null) {
			$otherSymbol = unserialize($otherSymbolStorage)['symbol'];
		}
		else $otherSymbol = null;
		
		// start new challenge
		if ($otherSymbol === null) {
			UserStorageHandler::getInstance()->update($user->userID, 'chatRpsChallenge', serialize([
				'userID' => $this->challengedUser->userID,
				'symbol' => $this->symbol
			]));
			
			// announce the challenge request
			$action = new MessageAction([], 'create', [
				'data' => [
					'roomID' => $room->roomID,
					'userID' => $user->userID,
					'username' => $user->username,
					'time' => TIME_NOW,
					'objectTypeID' => $this->getMessageObjectTypeID('net.dalang.chat.messageType.rps.challenge'),
					'payload' => serialize([
						'otherUsername' => $this->challengedUser->username,
						'ownUsername' => $user->username,
						'rpsMode' => $this->mode == 'rps'
					])
				],
				'updateTimestamp' => true,
				'grantPoints' => false
			]);
			$action->executeAction();
			
			// show some information to the challenged user
			$action = new MessageAction([], 'create', [
				'data' => [
					'roomID' => $room->roomID,
					'userID' => $this->challengedUser->userID,
					'username' => $this->challengedUser->username,
					'time' => TIME_NOW,
					'objectTypeID' => $this->getMessageObjectTypeID('net.dalang.chat.messageType.rps.challenge.info'),
					'payload' => serialize([
						'ownUsername' => $user->username,
						'rpsMode' => $this->mode == 'rps'
					])
				],
				'updateTimestamp' => true,
				'grantPoints' => false
			]);
			$action->executeAction();
		}
		// response to an existing challenge
		else {
			// play the actual RPS or RPSLS 'game'
			$status = 0;
			switch ($this->symbol) {
				case 'rock':
					if ($otherSymbol == 'scissors' || $otherSymbol == 'lizard') $status = 1;
					if ($otherSymbol == 'paper' || $otherSymbol == 'spock') $status = -1;
					break;
				case 'paper':
					if ($otherSymbol == 'rock' || $otherSymbol == 'spock') $status = 1;
					if ($otherSymbol == 'scissors' || $otherSymbol == 'lizard') $status = -1;
					break;
				case 'scissors':
					if ($otherSymbol == 'paper' || $otherSymbol == 'lizard') $status = 1;
					if ($otherSymbol == 'rock' || $otherSymbol == 'spock') $status = -1;
					break;
				case 'lizard':
					if ($otherSymbol == 'paper' || $otherSymbol == 'spock') $status = 1;
					if ($otherSymbol == 'scissors' || $otherSymbol == 'rock') $status = -1;
					break;
				case 'spock':
					if ($otherSymbol == 'scissors' || $otherSymbol == 'rock') $status = 1;
					if ($otherSymbol == 'paper' || $otherSymbol == 'lizard') $status = -1;
					break;
			}
			
			UserStorageHandler::getInstance()->reset([$user->userID, $this->challengedUser->userID], 'chatRpsChallenge');
			
			$action = new MessageAction([], 'create', [
				'data' => [
					'roomID' => $room->roomID,
					'userID' => $user->userID,
					'username' => $user->username,
					'time' => TIME_NOW,
					'objectTypeID' => $this->getMessageObjectTypeID('net.dalang.chat.messageType.rps.response'),
					'payload' => serialize([
						'ownUsername' => $user->username,
						'ownSymbol' => FontAwesomeIcon::fromValues($this->getSymbolIcon($this->symbol), false)->toHtml(),
						'ownSymbolName' => $this->symbol,
						'otherUsername' => $this->challengedUser->username,
						'otherSymbol' => FontAwesomeIcon::fromValues($this->getSymbolIcon($otherSymbol), false)->toHtml(),
						'otherSymbolName' => $otherSymbol,
						'status' => $status,
						'rpsMode' => $this->mode == 'rps'
					])
				],
				'updateTimestamp' => true,
				'grantPoints' => false
			]);
			$action->executeAction();
		}
	}
	
	/**
	 * Returns the FA-icon name for a specific symbol
	 * @param	string	$symbol
	 * @return	string
	 */
	protected function getSymbolIcon(string $symbol): string {
		return match ($symbol) {
			'rock' => 'hand-back-fist',
			'paper' => 'hand',
			'scissors' => 'hand-scissors',
			'lizard' => 'hand-lizard',
			'spock' => 'hand-spock',
			default => 'question'
		};
	}
}