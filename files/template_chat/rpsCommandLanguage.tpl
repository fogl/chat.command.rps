Language.addObject({
	'chat.messageType.net.dalang.chat.messageType.rps.cancel': '{lang __literal=true}chat.messageType.net.dalang.chat.messageType.rps.cancel{/lang}',
	'chat.messageType.net.dalang.chat.messageType.rps.challenge': '{lang __literal=true}chat.messageType.net.dalang.chat.messageType.rps.challenge{/lang}',
	'chat.messageType.net.dalang.chat.messageType.rps.challenge.info': '{lang __literal=true}chat.messageType.net.dalang.chat.messageType.rps.challenge.info{/lang}',
	'chat.messageType.net.dalang.chat.messageType.rps.response': '{lang __literal=true}chat.messageType.net.dalang.chat.messageType.rps.response{/lang}',
})

{jsphrase name='chat.command.rps.lizard'}
{jsphrase name='chat.command.rps.paper'}
{jsphrase name='chat.command.rps.rock'}
{jsphrase name='chat.command.rps.scissors'}
{jsphrase name='chat.command.rps.spock'}