{literal}
<script type="x-text/template" data-application="be.bastelstu.chat" data-template-name="net-dalang-chat-messageType-rps-challenge" data-template-includes="DeleteButton">
	<div class="chatMessageContainer inline">
		<div class="chatMessageSide">
			<div class="chatUserAvatar jsUserActionDropdown" data-user-id="{$author.userID}">
				<a href="{$author.link}">{@$author.image24}</a>
			</div>
			<time><a href="{$message.link}">{$message.formattedTime}</a></time>
		</div>
		<div class="chatMessageContent">
			<div class="chatMessageHeader">
				<span class="username">
					<a href="{$author.link}" class="jsUserActionDropdown" data-user-id="{$author.userID}">
						{@$author.coloredUsername}
					</a>
				</span>
				<small class="separatorLeft">
					<time><a href="{$message.link}">{$message.formattedTime}</a></time>
				</small>
			</div>
			<span class="chatMessageIcon">
				{icon name='hand-point-right'}
			</span>
			<div class="chatMessage">{@$author.coloredUsername} {lang}chat.messageType.{$message.objectType}{/lang}</div>
		</div>

		<ul class="buttonGroup buttonList smallButtons">
            {/literal}
            {if $__wcf->session->getPermission('mod.chat.canDelete')}
				{ldelim}include file=$t.DeleteButton}
            {/if}
            {literal}
		</ul>
	</div>
</script>

<script type="x-text/template" data-application="be.bastelstu.chat" data-template-name="net-dalang-chat-messageType-rps-response" data-template-includes="DeleteButton">
	<div class="chatMessageContainer inline">
		<div class="chatMessageSide">
			<div class="chatUserAvatar jsUserActionDropdown" data-user-id="{$author.userID}">
				<a href="{$author.link}">{@$author.image24}</a>
			</div>
			<time><a href="{$message.link}">{$message.formattedTime}</a></time>
		</div>
		<div class="chatMessageContent">
			<div class="chatMessageHeader">
				<span class="username">
					<a href="{$author.link}" class="jsUserActionDropdown" data-user-id="{$author.userID}">
						{@$author.coloredUsername}
					</a>
				</span>
				<small class="separatorLeft">
					<time><a href="{$message.link}">{$message.formattedTime}</a></time>
				</small>
			</div>
			<span class="chatMessageIcon">
				{icon name='hand-point-left'}
			</span>
			<div class="chatMessage">{@$author.coloredUsername} {lang}chat.messageType.{$message.objectType}{/lang}</div>
		</div>

		<ul class="buttonGroup buttonList smallButtons">
            {/literal}
            {if $__wcf->session->getPermission('mod.chat.canDelete')}
				{ldelim}include file=$t.DeleteButton}
            {/if}
            {literal}
		</ul>
	</div>
</script>

	<script type="x-text/template" data-application="be.bastelstu.chat" data-template-name="net-dalang-chat-messageType-rps-cancel">
		<div class="chatMessageContainer inline">
			<div class="chatMessageSide">
				<div class="chatUserAvatar jsUserActionDropdown" data-user-id="{$author.userID}">
					<a href="{$author.link}">{@$author.image24}</a>
				</div>
				<time><a href="{$message.link}">{$message.formattedTime}</a></time>
			</div>
			<div class="chatMessageContent">
				<div class="chatMessageHeader">
				<span class="username">
					<a href="{$author.link}" class="jsUserActionDropdown" data-user-id="{$author.userID}">
						{@$author.coloredUsername}
					</a>
				</span>
					<small class="separatorLeft">
						<time><a href="{$message.link}">{$message.formattedTime}</a></time>
					</small>
				</div>
				<span class="chatMessageIcon">
					{icon name='xmark'}
				</span>
				<div class="chatMessage">{lang}chat.messageType.{$message.objectType}{/lang}</div>
			</div>

			<ul class="buttonGroup buttonList smallButtons">

			</ul>
		</div>
	</script>

	<script type="x-text/template" data-application="be.bastelstu.chat" data-template-name="net-dalang-chat-messageType-rps-challenge-info">
		<div class="chatMessageContainer inline">
			<div class="chatMessageSide">
				<div class="chatUserAvatar jsUserActionDropdown" data-user-id="{$author.userID}">
					<a href="{$author.link}">{@$author.image24}</a>
				</div>
				<time><a href="{$message.link}">{$message.formattedTime}</a></time>
			</div>
			<div class="chatMessageContent">
				<div class="chatMessageHeader">
				<span class="username">
					<a href="{$author.link}" class="jsUserActionDropdown" data-user-id="{$author.userID}">
						{@$author.coloredUsername}
					</a>
				</span>
					<small class="separatorLeft">
						<time><a href="{$message.link}">{$message.formattedTime}</a></time>
					</small>
				</div>
				<span class="chatMessageIcon">
					{icon name='circle-exclamation'}
				</span>
				<div class="chatMessage">{lang}chat.messageType.{$message.objectType}{/lang}</div>
			</div>

			<ul class="buttonGroup buttonList smallButtons">

			</ul>
		</div>
	</script>
{/literal}