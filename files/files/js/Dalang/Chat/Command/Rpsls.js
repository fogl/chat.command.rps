/**
 * rpsls command
 * this is basically a copy of Rps.js
 * and yes, I am that lazy
 *
 * @author		Daniel (Keito) Lang
 * @copyright	2014-2023 da-lang.net
 * @license		da-lang.net Commercial License <https://da-lang.net/license/commercial.txt>
 * @package		net.dalang.chat.command.rps
 * @module		Dalang/Chat/Command/Rps
 */

define(['Bastelstu.be/Chat/Command', 'Bastelstu.be/Chat/Parser'], function (Command, Parser) {
	"use strict";

	const DEPENDENCIES = ['ProfileStore']

	class Rpsls extends Command {
		constructor(profileStore, id) {
			super(id)
			this.profileStore = profileStore
		}

		getParameterParser() {
			const Rock = Parser.C.string('rock')
				.thenLeft(Parser.Whitespace.rep())
				.thenRight(Parser.Username)
				.map((username) => {
					return { symbol: 'rock', username }
				})

			const Paper = Parser.C.string('paper')
				.thenLeft(Parser.Whitespace.rep())
				.thenRight(Parser.Username)
				.map((username) => {
					return { symbol: 'paper', username }
				})

			const Scissors = Parser.C.string('scissors')
				.thenLeft(Parser.Whitespace.rep())
				.thenRight(Parser.Username)
				.map((username) => {
					return { symbol: 'scissors', username }
				})

			const Lizard = Parser.C.string('lizard')
				.thenLeft(Parser.Whitespace.rep())
				.thenRight(Parser.Username)
				.map((username) => {
					return { symbol: 'lizard', username }
				})

			const Spock = Parser.C.string('spock')
				.thenLeft(Parser.Whitespace.rep())
				.thenRight(Parser.Username)
				.map((username) => {
					return { symbol: 'spock', username }
				})

			const Cancel = Parser.C.string('cancel').thenReturns({ symbol: 'cancel' })

			return Rock.or(Paper).or(Scissors).or(Lizard).or(Spock).or(Cancel)
		}

		*autocomplete(parameterString) {
			const Rock = Parser.C.string('rock');
			const Paper = Parser.C.string('paper');
			const Scissors = Parser.C.string('scissors');
			const Lizard = Parser.C.string('lizard');
			const Spock = Parser.C.string('spock');
			const Cancel = Parser.C.string('cancel');

			const symbolDone = Rock.or(Paper).or(Scissors).or(Lizard).or(Spock).or(Cancel)
				.thenLeft(Parser.Whitespace)

			const symbolCheck = symbolDone.parse(
				Parser.Streams.ofString(parameterString)
			)
			if (symbolCheck.isAccepted()) {

				let prefix = parameterString.substring(0, symbolCheck.offset)
				for (const userID of this.profileStore.getLastActivity()) {
					const user = this.profileStore.get(userID)
					//if (!user.username.startsWith(parameterString)) continue

					yield `${prefix}"${user.username.replace(/"/g, '""')}" `
				}
			}

			yield* ['rock ', 'paper ', 'scissors ', 'lizard ', 'spock ', 'cancel'].filter((item) =>
				item.startsWith(parameterString)
			)
		}
	}
	Rpsls.DEPENDENCIES = DEPENDENCIES

	return Rpsls
});