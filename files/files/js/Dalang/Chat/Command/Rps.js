/**
 * rps command
 *
 * @author		Daniel (Keito) Lang
 * @copyright	2014-2023 da-lang.net
 * @license		da-lang.net Commercial License <https://da-lang.net/license/commercial.txt>
 * @package		net.dalang.chat.command.rps
 * @module		Dalang/Chat/Command/Rps
 */

define(['Bastelstu.be/Chat/Command', 'Bastelstu.be/Chat/Parser'], function (Command, Parser) {
	"use strict";

	const DEPENDENCIES = ['ProfileStore']

	class Rps extends Command {
		constructor(profileStore, id) {
			super(id)
			this.profileStore = profileStore
		}

		getParameterParser() {
			const Rock = Parser.C.string('rock')
				.thenLeft(Parser.Whitespace.rep())
				.thenRight(Parser.Username)
				.map((username) => {
					return { symbol: 'rock', username }
				})

			const Paper = Parser.C.string('paper')
				.thenLeft(Parser.Whitespace.rep())
				.thenRight(Parser.Username)
				.map((username) => {
					return { symbol: 'paper', username }
				})

			const Scissors = Parser.C.string('scissors')
				.thenLeft(Parser.Whitespace.rep())
				.thenRight(Parser.Username)
				.map((username) => {
					return { symbol: 'scissors', username }
				})

			const Cancel = Parser.C.string('cancel').thenReturns({ symbol: 'cancel' })

			return Rock.or(Paper).or(Scissors).or(Cancel)
		}

		*autocomplete(parameterString) {
			const Rock = Parser.C.string('rock');
			const Paper = Parser.C.string('paper');
			const Scissors = Parser.C.string('scissors');
			const Cancel = Parser.C.string('cancel');

			const symbolDone = Rock.or(Paper).or(Scissors).or(Cancel)
				.thenLeft(Parser.Whitespace)

			const symbolCheck = symbolDone.parse(
				Parser.Streams.ofString(parameterString)
			)
			if (symbolCheck.isAccepted()) {

				let prefix = parameterString.substring(0, symbolCheck.offset)
				for (const userID of this.profileStore.getLastActivity()) {
					const user = this.profileStore.get(userID)
					//if (!user.username.startsWith(parameterString)) continue

					yield `${prefix}"${user.username.replace(/"/g, '""')}" `
				}
			}

			yield* ['rock ', 'paper ', 'scissors ', 'cancel'].filter((item) =>
				item.startsWith(parameterString)
			)
		}
	}
	Rps.DEPENDENCIES = DEPENDENCIES

	return Rps
});