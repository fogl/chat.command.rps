/**
 * rps cancel message type
 *
 * @author		Daniel (Keito) Lang
 * @copyright	2014-2023 da-lang.net
 * @license		da-lang.net Commercial License <https://da-lang.net/license/commercial.txt>
 * @package		net.dalang.chat.command.rps
 * @module		Dalang/Chat/MessageType/RpsCancel
 */

define(['Bastelstu.be/Chat/MessageType'], function (MessageType) {
	"use strict";

	class RpsCancel extends MessageType { }

	return RpsCancel
});