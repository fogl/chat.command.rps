/**
 * rps response message type
 *
 * @author		Daniel (Keito) Lang
 * @copyright	2014-2023 da-lang.net
 * @license		da-lang.net Commercial License <https://da-lang.net/license/commercial.txt>
 * @package		net.dalang.chat.command.rps
 * @module		Dalang/Chat/MessageType/RpsResponse
 */

define(['Bastelstu.be/Chat/MessageType'], function (MessageType) {
	"use strict";

	class RpsResponse extends MessageType { }

	return RpsResponse
});