/**
 * rps challenge message type
 *
 * @author		Daniel (Keito) Lang
 * @copyright	2014-2023 da-lang.net
 * @license		da-lang.net Commercial License <https://da-lang.net/license/commercial.txt>
 * @package		net.dalang.chat.command.rps
 * @module		Dalang/Chat/MessageType/RpsChallenge
 */

define(['Bastelstu.be/Chat/MessageType'], function (MessageType) {
	"use strict";

	class RpsChallenge extends MessageType { }

	return RpsChallenge
});